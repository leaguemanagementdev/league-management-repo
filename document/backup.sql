-- MySQL dump 10.10
--
-- Host: localhost    Database: LeagueManager
-- ------------------------------------------------------
-- Server version	5.0.19-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `game_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--


/*!40000 ALTER TABLE `game` DISABLE KEYS */;
LOCK TABLES `game` WRITE;
INSERT INTO `game` VALUES (1,'Badminton'),(2,'Soccer');
UNLOCK TABLES;
/*!40000 ALTER TABLE `game` ENABLE KEYS */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `location_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--


/*!40000 ALTER TABLE `location` DISABLE KEYS */;
LOCK TABLES `location` WRITE;
INSERT INTO `location` VALUES (1,'Yard A'),(2,'Yard B'),(3,'Yard C');
UNLOCK TABLES;
/*!40000 ALTER TABLE `location` ENABLE KEYS */;

--
-- Table structure for table `matchs`
--

DROP TABLE IF EXISTS `matchs`;
CREATE TABLE `matchs` (
  `matchs_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `result` int(11) default NULL,
  `score` varchar(255) default NULL,
  `timematch` time default NULL,
  `location_id` int(11) NOT NULL,
  `team1_id` int(11) NOT NULL,
  `team2_id` int(11) NOT NULL,
  PRIMARY KEY  (`matchs_id`),
  KEY `FK_sk4bp3sgejq46bnscbrp7ne9s` (`location_id`),
  KEY `FK_syppsdj3a789rkpe1db8sb2qw` (`team1_id`),
  KEY `FK_lpxipcn915xi03jtckwy4dhgr` (`team2_id`),
  CONSTRAINT `FK_8yctxx64caijokrinx1puw6fj` FOREIGN KEY (`matchs_id`) REFERENCES `matchs` (`matchs_id`),
  CONSTRAINT `FK_lpxipcn915xi03jtckwy4dhgr` FOREIGN KEY (`team2_id`) REFERENCES `team` (`team_id`),
  CONSTRAINT `FK_sk4bp3sgejq46bnscbrp7ne9s` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`),
  CONSTRAINT `FK_syppsdj3a789rkpe1db8sb2qw` FOREIGN KEY (`team1_id`) REFERENCES `team` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matchs`
--


/*!40000 ALTER TABLE `matchs` DISABLE KEYS */;
LOCK TABLES `matchs` WRITE;
INSERT INTO `matchs` VALUES (1,'Test Matchs',1,'1-2','03:00:00',1,2,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `matchs` ENABLE KEYS */;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
CREATE TABLE `player` (
  `player_id` int(11) NOT NULL auto_increment,
  `address` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `pathIcons` varchar(255) default NULL,
  `sex` int(11) default NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY  (`player_id`),
  KEY `FK_5q11flfd61t4n9eepixi8ltup` (`team_id`),
  CONSTRAINT `FK_5q11flfd61t4n9eepixi8ltup` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player`
--


/*!40000 ALTER TABLE `player` DISABLE KEYS */;
LOCK TABLES `player` WRITE;
INSERT INTO `player` VALUES (1,'Nga Bay Hau Giang','thuanpv','images/2.png',1,1),(2,'Ha Noi','viensinhvan','images/3.png',1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `player` ENABLE KEYS */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL auto_increment,
  `type` varchar(255) default NULL,
  PRIMARY KEY  (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--


/*!40000 ALTER TABLE `role` DISABLE KEYS */;
LOCK TABLES `role` WRITE;
INSERT INTO `role` VALUES (1,'ADMIN'),(2,'USER');
UNLOCK TABLES;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

--
-- Table structure for table `round`
--

DROP TABLE IF EXISTS `round`;
CREATE TABLE `round` (
  `round_id` int(11) NOT NULL auto_increment,
  `createDate` date default NULL,
  `name` varchar(255) default NULL,
  `tournament_id` int(11) NOT NULL,
  PRIMARY KEY  (`round_id`),
  KEY `FK_n1q8nvyumo9nhr0909akoegu0` (`tournament_id`),
  CONSTRAINT `FK_n1q8nvyumo9nhr0909akoegu0` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`tournament_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `round`
--


/*!40000 ALTER TABLE `round` DISABLE KEYS */;
LOCK TABLES `round` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `round` ENABLE KEYS */;

--
-- Table structure for table `score_detail`
--

DROP TABLE IF EXISTS `score_detail`;
CREATE TABLE `score_detail` (
  `scoreDetail_id` int(11) NOT NULL auto_increment,
  `draw` int(11) default NULL,
  `lost` int(11) default NULL,
  `win` int(11) default NULL,
  `matchs_id` int(11) default NULL,
  `team_id` int(11) default NULL,
  PRIMARY KEY  (`scoreDetail_id`),
  KEY `FK_hsi2ujtf053crul0nvoe17umx` (`matchs_id`),
  KEY `FK_96hnpk97a5jksy6wud9hharn8` (`team_id`),
  CONSTRAINT `FK_96hnpk97a5jksy6wud9hharn8` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`),
  CONSTRAINT `FK_hsi2ujtf053crul0nvoe17umx` FOREIGN KEY (`matchs_id`) REFERENCES `matchs` (`matchs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `score_detail`
--


/*!40000 ALTER TABLE `score_detail` DISABLE KEYS */;
LOCK TABLES `score_detail` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `score_detail` ENABLE KEYS */;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `team_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `tournament_id` int(11) NOT NULL,
  PRIMARY KEY  (`team_id`),
  KEY `FK_hxfef7vh4xgyf4gyfmff1awi7` (`tournament_id`),
  CONSTRAINT `FK_hxfef7vh4xgyf4gyfmff1awi7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`tournament_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--


/*!40000 ALTER TABLE `team` DISABLE KEYS */;
LOCK TABLES `team` WRITE;
INSERT INTO `team` VALUES (1,'Team A',1),(2,'Team B',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `team` ENABLE KEYS */;

--
-- Table structure for table `tournament`
--

DROP TABLE IF EXISTS `tournament`;
CREATE TABLE `tournament` (
  `tournament_id` int(11) NOT NULL auto_increment,
  `createDate` date default NULL,
  `name` varchar(255) default NULL,
  `numberTeam` int(11) default NULL,
  `game_id` int(11) NOT NULL,
  `typetournament_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`tournament_id`),
  KEY `FK_9t3t5011l4capu9eylm2cdt6` (`game_id`),
  KEY `FK_2ilnswpgbqyqne06tydxev28w` (`typetournament_id`),
  KEY `FK_s05taqhswl2h81wjhth15l9cn` (`user_id`),
  CONSTRAINT `FK_s05taqhswl2h81wjhth15l9cn` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FK_2ilnswpgbqyqne06tydxev28w` FOREIGN KEY (`typetournament_id`) REFERENCES `typetournament` (`typetournament_id`),
  CONSTRAINT `FK_9t3t5011l4capu9eylm2cdt6` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournament`
--


/*!40000 ALTER TABLE `tournament` DISABLE KEYS */;
LOCK TABLES `tournament` WRITE;
INSERT INTO `tournament` VALUES (1,'2016-05-19','Test Tournament',5,1,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `tournament` ENABLE KEYS */;

--
-- Table structure for table `typetournament`
--

DROP TABLE IF EXISTS `typetournament`;
CREATE TABLE `typetournament` (
  `typetournament_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`typetournament_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `typetournament`
--


/*!40000 ALTER TABLE `typetournament` DISABLE KEYS */;
LOCK TABLES `typetournament` WRITE;
INSERT INTO `typetournament` VALUES (1,'Round Robin'),(2,'Knock Out');
UNLOCK TABLES;
/*!40000 ALTER TABLE `typetournament` ENABLE KEYS */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL auto_increment,
  `password` varchar(255) default NULL,
  `pathIcons` varchar(255) default NULL,
  `sex` int(11) default NULL,
  `username` varchar(255) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--


/*!40000 ALTER TABLE `user` DISABLE KEYS */;
LOCK TABLES `user` WRITE;
INSERT INTO `user` VALUES (1,'12345','data/1.png',1,'pvthuan');
UNLOCK TABLES;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY  (`user_id`,`role_id`),
  KEY `FK_it77eq964jhfqtu54081ebtio` (`role_id`),
  CONSTRAINT `FK_apcc8lxk2xnug8377fatvbn04` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FK_it77eq964jhfqtu54081ebtio` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--


/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
LOCK TABLES `user_role` WRITE;
INSERT INTO `user_role` VALUES (1,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

