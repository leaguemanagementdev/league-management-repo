app.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'app/js/tournament/template/tournament.html',
		controller : 'TournamentController'
	}).when('/findTournament', {
		templateUrl : 'app/template/findTournament.html',
		controller : 'findTournament'
	}).when('/createuser', {
		templateUrl : 'app/template/createUser.html',
		controller : 'createUser'
	}).when('/loginuser', {
		templateUrl : 'app/template/loginUser.html',
		controller : 'loginUser'
	}).when('/showGame', {
		templateUrl : 'app/template/showGame.html',
		controller : 'showGame'
	}).when('/addGame', {
		templateUrl : 'app/template/addGame.html',
		controller : 'addGame'
	}).when('/team',{
		templateUrl: 'app/js/team/template/team.html',
		controller: 'TeamController'
	}).when('/player',{
		templateUrl: 'app/js/player/template/player.html',
		controller: 'PlayerController'
	})
	.otherwise({
		redirectTo : '/'
	});
} ]);