app.controller('PlayerController', ['$scope','PlayerService','$uibModal',
                                        function($scope,PlayerService, $uibModal){
	$scope.players = [];
	$scope.player = {};
	$scope.getAll = function() {
		PlayerService.getData()
	    .then(function(response) {
	    	$scope.players = response.data;
	    }, function(errResponse) {
	              console.error('Error while loading player');
	    	});
	    };
	$scope.getAll();   
	$scope.deleteTT = function(id)
    {
		PlayerService.deleteData(id).then(function(response){
    		$scope.getAll();
    	},function(errResponse){
    		console.log("Error while deleting player");
    		
    	});
    };
	$scope.open = function(id) {
		var modalInstance = $uibModal
				.open({
					animation : $scope.animationsEnabled,
					templateUrl : 'app/js/player/template/Modal.html',
					controller : 'ModalCtrlPlayer',
					resolve : {
						Action : function() {
							if (id == null || typeof id == 'undefined') {
								return "New";
							} else {
								return "Edit";
							}
							
						},
						ID : function(){
							if (id == null || typeof id == 'undefined') {
								return null;
							} else {
								return id;
							}
						}
					}
				});
	
		modalInstance.result.then(
				function() {
					$scope.getAll();
				}, function() {
					
				});
		};
}])
.controller('ModalCtrlPlayer', ['$scope','PlayerService','$uibModalInstance','Action','ID',
                          function($scope, PlayerService,$uibModalInstance, Action, ID){
	$scope.action = Action;
	function loadData(){
		if (ID) {
			PlayerService.getDataById(ID).then(function(respone){
				$scope.player = respone.data;
			});
		}
	};
	
	loadData();
	
	
	$scope.submit = function(){
		if (ID) {
			PlayerService.putData($scope.team, ID).then(function(response){
				$uibModalInstance.close(response.data);
			});
		}else {
			PlayerService.postData($scope.team).then(function(response){
				$uibModalInstance.close(response.data);
			});
		}
	};
	function getTeamtAll()
	{
		PlayerService.getDataTeam().then(function(response){
			 $scope.teams = response.data;
		 },function(errResponse)
		 {
			 console.log('Error');
		 });
	 }
	getTeamtAll();
	$scope.cancel = function()
	{
		$uibModalInstance.dismiss('Cancel');
	}
}]);