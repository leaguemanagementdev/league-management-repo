
app.factory('TeamService', ['$http', function($http) {
	var url = 'http://localhost:8080/LeagueManagement/api/';
	return {
		getData: function(id)
		{
			return $http.get(url+'team/all/'+id);
		},
		getDataById: function(id){
			return $http.get(url+'team/'+id);
		},
		postData: function(data)
		{
			return $http.post(url+'team/'+'new',data);
		}
		,
		putData: function(data,id)
		{
			return $http.put(url+'team/'+id,data);
		},
		deleteData: function(id)
		{
			return $http.delete(url+'team/'+id);
		},
		getDataT: function()
		{
			return $http.get(url+'tournament/'+'all');
		},
		getDataTT: function()
		{
			return $http.get(url+'typetournament/'+'all');
		}
		,
		getDataU: function()
		{
			return $http.get(url+'user/all');
		},
		getDataT: function()
		{
			return $http.get(url+'tournament/'+'all');
		}
	}
} ]);