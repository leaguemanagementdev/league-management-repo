app.controller('TeamController', ['$scope','TeamService','$uibModal',
                                        function($scope,TeamService, $uibModal){
	$scope.tournaments = [];
	$scope.teams = [];
	$scope.team = {};
	$scope.getAll = function() {
	    TeamService.getData()
	    .then(function(response) {
	    	$scope.teams = response.data;
	    }, function(errResponse) {
	              console.error('Error while loading team');
	    	});
	    };
	$scope.getAll();   
	$scope.deleteTT = function(id)
    {
    	TeamService.deleteData(id).then(function(response){
    		$scope.getAll();
    	},function(errResponse){
    		console.log("Error while deleting team");
    		
    	});
    };
	$scope.open = function(id) {
		var modalInstance = $uibModal
				.open({
					animation : $scope.animationsEnabled,
					templateUrl : 'app/js/team/template/Modal.html',
					controller : 'ModalCtrlTeam',
					resolve : {
						Action : function() {
							if (id == null || typeof id == 'undefined') {
								return "New";
							} else {
								return "Edit";
							}		
						},
						ID : function(){
							if (id == null || typeof id == 'undefined') {
								return null;
							} else {
								return id;
							}
						}
					}
				});
	
		modalInstance.result.then(
				function() {
					$scope.getAll();
				}, function() {
					
				});
		};
}])
.controller('ModalCtrlTeam', ['$scope','TeamService','$uibModalInstance','Action','ID',
                          function($scope, TeamService,$uibModalInstance, Action, ID){
	$scope.action = Action;
	function loadData(){
		if (ID) {
			TeamService.getDataById(ID).then(function(respone){
				$scope.team = respone.data;
			});
		}
	};
	
	loadData();
	
	
	$scope.submit = function(){
		if (ID) {
			TeamService.putData($scope.team, ID).then(function(response){
				$uibModalInstance.close(response.data);
			});
		}else {
			TeamService.postData($scope.team).then(function(response){
				$uibModalInstance.close(response.data);
			});
		}
	};
	function getTeamAll()
	 {
		 TeamService.getDataT().then(function(response){
			 $scope.tournaments = response.data;
		 },function(errResponse){
			 console.log('Error');
			 
		 });
	 }
	getTeamAll();
	$scope.cancel = function()
	{
		$uibModalInstance.dismiss('Cancel');
	}
}]);
