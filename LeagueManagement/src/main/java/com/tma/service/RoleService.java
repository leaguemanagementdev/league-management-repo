package com.tma.service;

import java.util.List;

import com.tma.model.Role;

public interface RoleService {
	Role findById(int id);
	Role findByName(String name);
	Role saveRole(Role ur);
	void updateRole(Role ur);
	void deleteRole(int id);
	void deleteRole(String name);
	List<Role>listAll();
	List<Role> listAll(int first, int second);
}
