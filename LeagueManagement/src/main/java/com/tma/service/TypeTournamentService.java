package com.tma.service;

import java.util.List;
import java.util.Set;

import com.tma.model.Tournament;
import com.tma.model.TypeTournament;

public interface TypeTournamentService {
	TypeTournament findById(int id);

	TypeTournament findByName(String name);

	TypeTournament saveTypeTournament(TypeTournament typeTournament);

	void updateTypeTournament(TypeTournament typeTournament);

	void deleteTypeTournament(String name);

	void deleteTypeTournament(int id);

	List<TypeTournament> listAllTypeTournament();

	List<TypeTournament> listAllTypeTournament(int first, int second);

	List<Tournament> findByIdTournament(int id);
}
