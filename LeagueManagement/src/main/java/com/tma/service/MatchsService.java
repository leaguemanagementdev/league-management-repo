package com.tma.service;

import java.util.List;

import com.tma.model.Matchs;

public interface MatchsService
{
	Matchs findMatchsById(int id);
	Matchs findMatchsByName(String name);
	Matchs saveMatchs(Matchs matchs);
	void updateMatchs(Matchs matchs);
	void deleteMatchsById(int id);
	void deleteMatchsByName(String name);
	List<Matchs> listAllMatchs();
	List<Matchs> listAllMatchs(int first, int second);
}
