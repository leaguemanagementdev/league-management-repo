package com.tma.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tma.dao.LocationDao;
import com.tma.model.Location;
import com.tma.model.Matchs;
import com.tma.service.LocationService;
@Service(value = "locationService")
public class LocationServiceImpl implements LocationService
{

	@Autowired
	@Qualifier("locationDao")
	LocationDao locationDao;
	public Location findById(int id) {
		return locationDao.findById(id);
	}

	public Location findByName(String name) {
		return locationDao.findByName(name);
	}

	public Location saveLocation(Location location) {
		int id = locationDao.saveLocation(location);
		if(id>0)
		{
			return locationDao.findById(id);
		}
		else
		{
			return null;
		}
	}

	public void updateLocation(Location location) {
		locationDao.updateLocation(location);
	}

	public void deleteLocation(String name) {
		locationDao.deleteLocation(name);
	}

	public void deleteLocation(int id) {
		locationDao.deleteLocation(id);
	}

	public List<Matchs> getMatchsById(int id) {
		return locationDao.getMatchsById(id);
	}

	public List<Location> getAllLocation() {
		return locationDao.listAllLocation();
	}

	public List<Location> getAllLocation(int first, int second) {
		return locationDao.listAllLocation(first, second);
	}

}
