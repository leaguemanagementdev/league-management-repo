package com.tma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tma.dao.TournamentDao;
import com.tma.model.Team;
import com.tma.model.Tournament;
import com.tma.service.TournamentService;

@Service(value = "tournamentService")
public class TournamentServiceImpl implements TournamentService {
	@Autowired
	@Qualifier(value = "tournamentDao")
	TournamentDao tournamentDao;

	public Tournament findTournamentById(int id) {
		return tournamentDao.findTournamentById(id);
	}

	public Tournament findTournamentByName(String name) {
		return tournamentDao.findTornamentByName(name);
	}

	public Tournament saveTournament(Tournament tournament) {
		int id = tournamentDao.saveTournament(tournament);
		if (id > 0) {
			return tournamentDao.findTournamentById(id);
		} else {
			return null;
		}
	}

	public void updateTournament(Tournament tournament) {
		tournamentDao.updateTournament(tournament);
	}

	public void deleteTournament(int id) {
		tournamentDao.deleteTournamentById(id);
	}

	public void deleteTournament(String name) {
		tournamentDao.deleteTournamentByName(name);
	}

	public List<Tournament> getAllTournament() {
		return tournamentDao.listAllTournament();
	}

	public List<Tournament> getAllTournamnet(int first, int second) {
		return tournamentDao.listAllTournament(first, second);
	}
}
