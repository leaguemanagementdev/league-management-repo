package com.tma.service.impl;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tma.dao.TypeTournamentDao;
import com.tma.model.Tournament;
import com.tma.model.TypeTournament;
import com.tma.service.TypeTournamentService;

@Service(value = "typeTournamentService")
public class TypeTournamentServiceImpl implements TypeTournamentService {
	@Autowired
	@Qualifier(value = "typetournamentDao")
	TypeTournamentDao typeTournamentDao;

	public TypeTournament findById(int id) {
		return typeTournamentDao.findById(id);
	}

	public TypeTournament findByName(String name) {
		return typeTournamentDao.findByName(name);
	}

	public TypeTournament saveTypeTournament(TypeTournament typeTournament) {
		int id = typeTournamentDao.saveTypeTournament(typeTournament);
		if (id > 0) {
			return typeTournamentDao.findById(id);
		} else {
			return null;
		}
	}

	public void updateTypeTournament(TypeTournament typeTournament) {
		typeTournamentDao.updateTypeTournament(typeTournament);
	}

	public void deleteTypeTournament(String name) {
		typeTournamentDao.deleteTypeTournament(name);
	}

	public void deleteTypeTournament(int id) {
		typeTournamentDao.deleteTypeTournament(id);
	}

	public List<TypeTournament> listAllTypeTournament() {
		return typeTournamentDao.listAllTypeTournament();
	}

	public List<Tournament> findByIdTournament(int id) {
		return typeTournamentDao.findByIdTournament(id);
	}

	public List<TypeTournament> listAllTypeTournament(int first, int second) {
		return typeTournamentDao.listAllTypeTournament(first, second);
	}
}
