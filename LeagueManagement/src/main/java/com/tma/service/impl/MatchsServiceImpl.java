package com.tma.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tma.dao.MatchsDao;
import com.tma.model.Matchs;
import com.tma.service.MatchsService;
@Service(value="matchsService")
public class MatchsServiceImpl implements MatchsService
{
	@Autowired
	//@Qualifier(value="matchsDao")
	MatchsDao matchsDao;

	public Matchs findMatchsById(int id) {
		return matchsDao.findMatchsById(id);
	}

	public Matchs findMatchsByName(String name) {
		return matchsDao.findMatchsByName(name);
	}

	public Matchs saveMatchs(Matchs matchs) {
		int id = matchsDao.saveMatchs(matchs);
		if(id>0)
		{
			return matchsDao.findMatchsById(id);
		}
		else
		{
			return null;
		}
	}

	public void updateMatchs(Matchs matchs) {
		matchsDao.updateMatchs(matchs);
	}

	public void deleteMatchsById(int id) {
		matchsDao.deleteMatchsById(id);
	}

	public void deleteMatchsByName(String name) {
		matchsDao.deleteMatchsByName(name);
	}

	public List<Matchs> listAllMatchs() {
		return matchsDao.listAllMatchs();
	}

	public List<Matchs> listAllMatchs(int first, int second) {
		return matchsDao.listAllMatchs(first, second);
	}
	
	
	
}
