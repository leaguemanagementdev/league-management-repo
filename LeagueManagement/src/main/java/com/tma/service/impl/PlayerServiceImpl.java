package com.tma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tma.dao.PlayerDao;
import com.tma.model.Player;
import com.tma.service.PlayerService;

import sun.net.www.content.text.plain;

@Service(value = "playerService")
public class PlayerServiceImpl implements PlayerService
{
	@Autowired
	@Qualifier(value="playerDao")
	PlayerDao playerDao;

	public Player findById(int id) {
		return playerDao.findById(id);
	}

	public Player findByName(String name) {
		return playerDao.findByName(name);
	}

	public Player savePlayer(Player player) {
		int id = playerDao.savePlayer(player);
		if(id>0)
		{
			return playerDao.findById(id);
		}
		else
		{
			return null;
		}
	}

	public void deletePlayer(int id) {
		playerDao.deletePlayer(id);
	}

	public void deletePlayer(String name) {
		playerDao.deletePlayer(name);
	}

	public List<Player> listAllPlayer(int id) {
		return playerDao.listAllPlayer(id);
	}

	public void updatePlayer(Player player) {
		playerDao.updatePlayer(player);
	}

	public List<Player> listAllPlayer(int first, int second) {
		return  playerDao.listAllPlayer(first, second);
	}
	
	
}
