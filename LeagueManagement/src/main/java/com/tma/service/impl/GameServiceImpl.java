package com.tma.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tma.dao.GameDao;
import com.tma.dao.TournamentDao;
import com.tma.model.Game;
import com.tma.service.GameService;
@Service("gameService")
public class GameServiceImpl implements GameService
{
	@Autowired
//	@Qualifier("gameDao")
	GameDao gameDao;

	public Game findById(int id) {
		return gameDao.findById(id);
	}
	
	public Game findByName(String name) {
		return gameDao.findByName(name);
	}

	public Game saveGame(Game game) {
		int id = gameDao.saveGame(game);
		if(id>0)
		{
			return gameDao.findById(id);
		}
		else
		{
			return null;
		}
	}
	
	public void updateGame(Game game) {
		gameDao.updateGame(game);
	}
	
	public void deleteGame(String name) {
		gameDao.deleteGame(name);
	}

	public void deleteGame(int id) {
		gameDao.deleteGame(id);
	}
	
	public List<Game> listAllGame() {
		return gameDao.listAllGame();
	}

	public List<Game> listAllGame(int first, int second) {
		return gameDao.listAllGame(first, second);
	}
	
}
