package com.tma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.UsesSunHttpServer;
import org.springframework.stereotype.Service;

import com.tma.dao.UserDao;
import com.tma.model.User;
import com.tma.service.UserService;
@Service(value = "userService")
public class UserServiceImpl implements UserService
{

	@Autowired
	@Qualifier(value = "userDao")
	UserDao userDao;
	public boolean checkLogin(String username, String password) {
		if(userDao.checkLogin(username, password))
			return true;
		else
			return false;
	}

	public User findUserByName(String name) {
		return userDao.findUserByName(name);
	}

	public User findUserById(int id) {
		return userDao.findUserById(id);
	}

	public boolean checkUserExist(String username) {
		if(userDao.checkUserExist(username))
			return true;
		else
			return false;
	}

	public List<User> getAllUser() {
		return userDao.getAllUser();
	}

	public User saveUser(User user) {
		int id = userDao.saveUser(user);
		if(id>0)
		{
			return userDao.findUserById(id);
		}
		else
		{
			return null;
		}
	}

	public void updateUser(User user) {
		userDao.updateUser(user);
	}

	public void deleteUser(String name) {
		userDao.deleteUser(name);
	}

	public void deleteUser(int id) {
		userDao.deleteUser(id);
	}

	public List<User> getAllUser(int first, int second)
	{
		return userDao.getAllUser(first, second);
	}

}
