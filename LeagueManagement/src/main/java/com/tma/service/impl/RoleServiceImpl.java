package com.tma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tma.dao.RoleDao;
import com.tma.model.Role;
import com.tma.service.RoleService;

@Service(value ="roleService")
public class RoleServiceImpl implements RoleService
{
	@Autowired
	RoleDao roleDao;

	public Role findById(int id) {
		return roleDao.findById(id);
	}

	public Role findByName(String name) {
		return roleDao.findByName(name);
	}

	public Role saveRole(Role ur) {
		int id = roleDao.saveRole(ur);
		if(id>0)
		{
			return roleDao.findById(id);
		}
		else
		{
			return null;
		}
	}

	public void updateRole(Role ur) {
		roleDao.updateRole(ur);
	}

	public void deleteRole(int id) {
		roleDao.deleteRole(id);
	}

	public void deleteRole(String name) {
		roleDao.deleteRole(name);
	}

	public List<Role> listAll() {
		return roleDao.listAll();
	}

	public List<Role> listAll(int first, int second) {
		return roleDao.listAll(first, second);
	}
	
	
}
