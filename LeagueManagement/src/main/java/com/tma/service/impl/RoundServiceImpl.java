package com.tma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tma.dao.RoundDao;
import com.tma.model.Round;
import com.tma.service.RoundService;
@Service(value="roundService")
public class RoundServiceImpl implements RoundService
{
	@Autowired
	@Qualifier(value="roundDao")
	RoundDao roundDao;

	public Round findRoundById(int id) {
		return roundDao.findRoundById(id);
	}

	public Round findRoundByName(String name) {
		return roundDao.findRoundByName(name);
	}

	public Round saveRound(Round round) {
		int id = roundDao.saveRound(round);
		if(id>0)
		{
			return roundDao.findRoundById(id);
		}
		else
		{
			return null;
		}
	}

	public void deleteRounByIdd(int id) {
		roundDao.deleteRounByIdd(id);
	}

	public void deleteRoundByName(String name) {
		roundDao.deleteRoundByName(name);
	}

	public void updateRound(Round round) {
		roundDao.updateRound(round);
	}

	public List<Round> listAllRound() {
		return roundDao.listAllRound();
	}

	public List<Round> listAllRound(int first, int second) {
		return roundDao.listAllRound(first, second);
	}
	
}
