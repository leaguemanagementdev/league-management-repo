package com.tma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tma.dao.TeamDao;
import com.tma.model.Team;
import com.tma.service.TeamService;

@Service(value = "teamService")
public class TeamServiceImpl implements TeamService {
	@Autowired
	@Qualifier(value = "teamDao")
	TeamDao teamDao;

	public TeamDao getTeamDao() {
		return teamDao;
	}

	public void setTeamDao(TeamDao teamDao) {
		this.teamDao = teamDao;
	}

	public Team findTeamById(int id) {
		return teamDao.findByTeamId(id);
	}

	public Team findTeamByName(String name) {
		return teamDao.findByTeamName(name);
	}

	public Team saveTeam(Team team) {
		int id = teamDao.saveTeam(team);
		if (id > 0) {
			return teamDao.findByTeamId(id);
		} else {
			return null;
		}
	}

	public void updateTeam(Team team) {
		teamDao.updateTeam(team);
	}

	public void deleteTeamById(int id) {
		teamDao.deleteTeamById(id);
	}

	public void deleteTeamByName(String name) {
		teamDao.deleteTeamByName(name);
	}

	public List<Team> listAllTeam(int id) {
		return teamDao.listAll(id);
	}

	public List<Team> listAllTeam(int first, int second) {
		return teamDao.listAll(first, second);
	}

	public List<Team> listAll() {
		return teamDao.listAll();
	}

}
