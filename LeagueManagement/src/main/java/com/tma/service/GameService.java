package com.tma.service;

import java.util.List;

import com.tma.model.Game;

public interface GameService
{
	Game findById(int id);
	Game findByName(String name);
	Game saveGame(Game game);
	void updateGame(Game game);
	void deleteGame(String name);
	void deleteGame(int id);
	List<Game> listAllGame();
	List<Game> listAllGame(int first, int second);
}
