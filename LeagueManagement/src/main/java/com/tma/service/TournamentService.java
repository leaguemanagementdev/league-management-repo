package com.tma.service;

import java.util.List;

import com.tma.model.Team;
import com.tma.model.Tournament;

public interface TournamentService {
	Tournament findTournamentById(int id);

	Tournament findTournamentByName(String name);

	Tournament saveTournament(Tournament tournament);

	void updateTournament(Tournament tournament);

	void deleteTournament(int id);

	void deleteTournament(String name);

	List<Tournament> getAllTournament();

	List<Tournament> getAllTournamnet(int first, int second);
}
