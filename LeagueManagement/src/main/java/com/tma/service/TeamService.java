package com.tma.service;

import java.util.List;

import com.tma.model.Team;

public interface TeamService
{
	Team findTeamById(int id);
	Team findTeamByName(String name);
	Team saveTeam(Team team);
	void updateTeam(Team team);
	void deleteTeamById(int id);
	void deleteTeamByName(String id);
	List<Team> listAll();
	List<Team> listAllTeam(int id);
	List<Team> listAllTeam(int first, int second);
}
