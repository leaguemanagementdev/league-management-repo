package com.tma.service;

import java.util.List;

import com.tma.model.Player;

public interface PlayerService
{
	Player findById(int id);
	Player findByName(String name);
	Player savePlayer(Player player);
	void deletePlayer(int id);
	void deletePlayer(String name);
	void updatePlayer(Player player);
	List<Player> listAllPlayer(int id);
	List<Player> listAllPlayer(int first, int second);
}
