package com.tma.service;

import java.util.List;

import com.tma.model.Round;

public interface RoundService
{
	Round findRoundById(int id);
	Round findRoundByName(String name);
	Round saveRound(Round round);
	void deleteRounByIdd(int id);
	void deleteRoundByName(String name);
	void updateRound(Round round);
	List<Round> listAllRound();
	List<Round> listAllRound(int first, int second);
}
