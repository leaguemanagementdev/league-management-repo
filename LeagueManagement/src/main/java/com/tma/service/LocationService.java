package com.tma.service;

import java.util.List;
import java.util.Set;

import com.tma.model.Location;
import com.tma.model.Matchs;

public interface LocationService {
	Location findById(int id);
	Location findByName(String name);
	Location saveLocation(Location location);
	void updateLocation(Location location);
	void deleteLocation(String name);
	void deleteLocation(int id);
	List<Matchs> getMatchsById(int id);
	List<Location> getAllLocation();
	List<Location> getAllLocation(int first, int second);
}
