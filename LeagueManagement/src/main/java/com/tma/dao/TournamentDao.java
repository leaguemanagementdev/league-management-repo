package com.tma.dao;

import java.util.List;

import com.tma.model.Team;
import com.tma.model.Tournament;

public interface TournamentDao
{
	Tournament findTournamentById(int id);
	Tournament findTornamentByName(String name);
	int saveTournament(Tournament tournament);
	void updateTournament(Tournament tournament);
	void deleteTournamentById(int id);
	void deleteTournamentByName(String name);
	List<Tournament> listAllTournament();
	List<Tournament> listAllTournament(int first, int second);
}
