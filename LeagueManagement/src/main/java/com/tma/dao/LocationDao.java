package com.tma.dao;

import java.util.List;
import java.util.Set;

import com.tma.model.Location;
import com.tma.model.Matchs;

public interface LocationDao
{
	Location findById(int id);
	Location findByName(String name);
	int saveLocation(Location location);
	void updateLocation(Location location);
	void deleteLocation(String name);
	void deleteLocation(int id);
	List<Matchs> getMatchsById(int id);
	List<Location> listAllLocation();
	List<Location> listAllLocation(int first, int second);
}
