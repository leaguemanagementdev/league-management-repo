package com.tma.dao;

public interface Dao
{
	<T> int save(T t);
    <T> void update(T t);
    <T> void delete(T t);
}
