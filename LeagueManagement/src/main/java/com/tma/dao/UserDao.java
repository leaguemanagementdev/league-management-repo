package com.tma.dao;

import java.util.List;

import com.tma.model.User;

public interface UserDao
{
	boolean checkLogin(String username, String password);
    User findUserByName(String name);
    User findUserById(int id);
    boolean checkUserExist(String username);
    List<User> getAllUser();
    List<User> getAllUser(int first, int second);
    int saveUser(User user);
    void updateUser(User user);
    void deleteUser(String name);
    void deleteUser(int id);
}
