package com.tma.dao;

import java.util.List;

import com.tma.model.Round;

public interface RoundDao
{
	Round findRoundById(int id);
	Round findRoundByName(String name);
	int saveRound(Round round);
	void deleteRounByIdd(int id);
	void deleteRoundByName(String name);
	void updateRound(Round round);
	List<Round> listAllRound();
	List<Round> listAllRound(int first, int second);
}
