package com.tma.dao;

import java.util.List;

import com.tma.model.Player;

public interface PlayerDao
{
	Player findById(int id);
	Player findByName(String name);
	int savePlayer(Player player);
	void deletePlayer(int id);
	void deletePlayer(String name);
	List<Player> listAllPlayer(int id);
	List<Player> listAllPlayer(int first, int second);
	void updatePlayer(Player player);
}
