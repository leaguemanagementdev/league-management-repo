package com.tma.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.PlayerDao;
import com.tma.model.Player;
@Repository(value = "playerDao")
@Transactional
public class PlayerDaoImpl extends DaoImpl implements PlayerDao
{

	public Player findById(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Player.class);
		criteria.add(Restrictions.eq("id", id));
		Player player = (Player)criteria.uniqueResult();
		return player;
	}

	public Player findByName(String name)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Player.class);
		criteria.add(Restrictions.eq("name", name));
		Player player = (Player)criteria.uniqueResult();
		return player;
	}

	public int savePlayer(Player player)
	{
		return save(player);
	}

	public void deletePlayer(int id)
	{
		delete(id);
	}

	public void deletePlayer(String name)
	{
		deletePlayer(name);
	}

	public List<Player> listAllPlayer(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Player.class);
		criteria.add(Restrictions.eq("team.id", id));
		List<Player> players = criteria.list();
		return players;
	}

	public void updatePlayer(Player player) {
		update(player);
	}

	public List<Player> listAllPlayer(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Player.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<Player> players = criteria.list();
		return players;
	}
}
