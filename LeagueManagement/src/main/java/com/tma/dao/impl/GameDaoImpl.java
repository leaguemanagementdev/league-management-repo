package com.tma.dao.impl;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.GameDao;
import com.tma.model.Game;
import com.tma.model.Tournament;
@Repository("gameDao")
@Transactional
public class GameDaoImpl extends DaoImpl  implements GameDao
{

	public Game findById(int id) {
		Session session = getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria(Game.class);
		criteria.add(Restrictions.eq("id", id));
		Game game = (Game)criteria.uniqueResult();
		return game;
	}

	public Game findByName(String name) {
		Session session = getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria(Game.class);
		criteria.add(Restrictions.eq("name", name));
		Game game = (Game)criteria.uniqueResult();
		return game;
	}

	public int saveGame(Game game) 
	{
		return save(game);
	}

	public void updateGame(Game game) {
		update(game);
	}

	public void deleteGame(String name) {
		delete(findByName(name));
	}

	public void deleteGame(int id) {
		delete(findById(id));
	}

	public List<Game> listAllGame() {
		Session session = getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria(Game.class);
		List<Game> games = criteria.list();
		return games;
	}

	public List<Game> listAllGame(int first, int second) {
		Session session = getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria(Game.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<Game> games = criteria.list();
		return games;
	}
	
}
