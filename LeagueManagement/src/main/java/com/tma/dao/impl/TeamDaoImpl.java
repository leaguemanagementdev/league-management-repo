package com.tma.dao.impl;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.TeamDao;
import com.tma.model.Location;
import com.tma.model.Player;
import com.tma.model.ScoreDetail;
import com.tma.model.Team;
@Repository(value = "teamDao")
@Transactional
public class TeamDaoImpl extends DaoImpl implements TeamDao
{

	public Team findByTeamId(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Team.class);
		criteria.add(Restrictions.eq("id", id));
		Team team = (Team)criteria.uniqueResult();
		return team;
	}

	public Team findByTeamName(String name)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Team.class);
		criteria.add(Restrictions.eq("name", name));
		Team team = (Team)criteria.uniqueResult();
		return team;
	}

	public int saveTeam(Team team)
	{
		return save(team);
	}

	public void deleteTeamById(int id)
	{
		delete(findByTeamId(id));
	}

	public void deleteTeamByName(String name)
	{
		delete(findByTeamName(name));
	}

	public void updateTeam(Team team)
	{
		update(team);
	}

	public List<Player> findPlayerById(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Team.class);
		criteria.add(Restrictions.eq("id", id));
		Team team = (Team)criteria.uniqueResult();
		return team.getPlayer();
	}

	public List<ScoreDetail> findScoreDetailById(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Team.class);
		criteria.add(Restrictions.eq("id", id));
		Team team = (Team)criteria.uniqueResult();
		return team.getScoreDetail();
	}

	public List<Team> listAll(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Team.class);
		criteria.add(Restrictions.eq("tournament2.id", id));
		List<Team> teams = criteria.list();
		return teams;
	}

	public List<Team> listAll(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Team.class);
		List<Team> teams = criteria.list();
		return teams;
	}

	public List<Team> listAll() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Team.class);
		List<Team> teams = criteria.list();
		return teams;
	}

}
