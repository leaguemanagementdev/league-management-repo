package com.tma.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tma.dao.Dao;

public class DaoImpl implements Dao
{
	@Autowired
	SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public <T> int save(T t) {
		return (Integer) getSessionFactory().getCurrentSession().save(t);
		
	}
	public <T> void update(T t) {
		getSessionFactory().getCurrentSession().update(t);
	}

	public <T> void delete(T t) {
		getSessionFactory().getCurrentSession().delete(t);
	}
}
