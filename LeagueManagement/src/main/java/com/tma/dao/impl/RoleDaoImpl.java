package com.tma.dao.impl;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.RoleDao;
import com.tma.model.Role;
@Repository(value = "roleDao")
@Transactional
public class RoleDaoImpl extends DaoImpl implements RoleDao
{

	public Role findById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Role.class);
		criteria.add(Restrictions.eq("id", id));
		Role role = (Role) criteria.uniqueResult();
		return role;
	}

	public Role findByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		criteria.add(Restrictions.eq("name", name));
//		UserRole userRole =(UserRole) criteria.list().get(0);// index outofbound exception
		Role role = (Role) criteria.uniqueResult();
		return role;
	}

	public int saveRole(Role ur) {
		return save(ur);
	}

	public void updateRole(Role ur) {
		update(ur);
	}

	public void deleteRole(int id) {
		delete(id);
	}

	public void deleteRole(String name) {
		delete(name);
	}

	public List<Role> listAll() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		List<Role> roles = criteria.list();
		return roles;
	}

	public List<Role> listAll(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<Role> roles = criteria.list();
		return roles;
	}

}
