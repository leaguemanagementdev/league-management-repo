package com.tma.dao.impl;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.LocationDao;
import com.tma.model.Location;
import com.tma.model.Matchs;
@Repository(value = "locationDao")
@Transactional
public class LocationDaoImpl extends DaoImpl implements LocationDao
{
	public Location findById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Location.class);
		criteria.add(Restrictions.eq("id", id));
		Location location = (Location)criteria.uniqueResult();
		return location;
	}

	public Location findByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Location.class);
		criteria.add(Restrictions.eq("name", name));
		Location location = (Location)criteria.uniqueResult();
		return location;
	}

	public int saveLocation(Location location) {
		return save(location);
	}

	public void updateLocation(Location location) {
		update(location);
	}

	public void deleteLocation(String name) {
		delete(findByName(name));
	}

	public void deleteLocation(int id) {
		delete(findById(id));
	}

	public List<Location> listAllLocation() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Location.class);
		List<Location> locations = criteria.list();
		return locations;

	}

	public List<Location> listAllLocation(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Location.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<Location> locations = criteria.list();
		return locations;
	}

	public List<Matchs> getMatchsById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
