package com.tma.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.TournamentDao;
import com.tma.model.Team;
import com.tma.model.Tournament;

@Repository(value="tournamentDao")
@Transactional
public class TournamentDaoImpl extends DaoImpl implements TournamentDao
{
	public Tournament findTournamentById(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Tournament.class);
		criteria.add(Restrictions.eq("id", id));
		Tournament tournament = (Tournament)criteria.uniqueResult();
		return tournament;
	}

	public Tournament findTornamentByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Tournament.class);
		criteria.add(Restrictions.eq("name", name));
		Tournament tournament = (Tournament)criteria.uniqueResult();
		return tournament;
	}

	public int saveTournament(Tournament tournament) {
		return save(tournament);
	}

	public void updateTournament(Tournament tournament) {
		update(tournament);
	}

	public void deleteTournamentById(int id) {
		delete(findTournamentById(id));
	}

	public void deleteTournamentByName(String name) {
		deleteTournamentByName(name);
	}

	public List<Tournament> listAllTournament() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Tournament.class);
		List<Tournament> tournaments = criteria.list();
		return tournaments;
	}

	public List<Tournament> listAllTournament(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria =session.createCriteria(Tournament.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<Tournament> tournaments = criteria.list();
		return tournaments;
	}
	
}
