package com.tma.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.RoundDao;
import com.tma.model.Round;
@Repository(value="roundDao")
@Transactional
public class RoundDaoImpl extends DaoImpl implements RoundDao
{

	public Round findRoundById(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Round.class);
		criteria.add(Restrictions.eq("id", id));
		Round round = (Round)criteria.uniqueResult();
		return round;
	}

	public Round findRoundByName(String name)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Round.class);
		criteria.add(Restrictions.eq("name", name));
		Round round = (Round)criteria.uniqueResult();
		return round;
	}

	public int saveRound(Round round)
	{
		return save(round);
	}

	public void deleteRounByIdd(int id)
	{
		deleteRounByIdd(id);
	}

	public void deleteRoundByName(String name)
	{
		deleteRoundByName(name);
	}

	public void updateRound(Round round)
	{
		updateRound(round);
	}

	public List<Round> listAllRound() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Round.class);
		List<Round> rounds = criteria.list();
		return rounds;
	}

	public List<Round> listAllRound(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Round.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<Round> rounds = criteria.list();
		return rounds;
	}

}
