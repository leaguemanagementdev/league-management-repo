package com.tma.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.MatchsDao;
import com.tma.model.Matchs;

@Repository(value="matchsDao")
public class MatchsDaoImpl extends DaoImpl implements MatchsDao {

	public Matchs findMatchsById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Matchs.class);
		criteria.add(Restrictions.eq("id", id));
//		Matchs matchs = (Matchs) criteria.list().get(0);
		Matchs matchs = (Matchs)criteria.uniqueResult();
		return matchs;
	}

	public Matchs findMatchsByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Matchs.class);
		criteria.add(Restrictions.eq("name", name));
		Matchs matchs = (Matchs) criteria.uniqueResult();
		return matchs;
	}

	public int saveMatchs(Matchs matchs) {
		return save(matchs);
	}

	public void updateMatchs(Matchs matchs) {
		update(matchs);
	}

	public void deleteMatchsById(int id) {
		deleteMatchsById(id);
	}

	public void deleteMatchsByName(String name) {
		deleteMatchsByName(name);
	}

	public List<Matchs> listAllMatchs() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Matchs.class);
		List<Matchs> matchs = criteria.list();
		return matchs;
	}

	public List<Matchs> listAllMatchs(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Matchs.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<Matchs> matchs = criteria.list();
		return matchs;
	}

}
