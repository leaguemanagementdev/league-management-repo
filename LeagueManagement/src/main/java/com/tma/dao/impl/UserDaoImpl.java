package com.tma.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.UserDao;
import com.tma.model.User;
@Repository(value = "userDao")
@Transactional
public class UserDaoImpl extends DaoImpl implements UserDao
{
	public boolean checkLogin(String username, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	public User findUserByName(String name) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		User user = (User)criteria.uniqueResult();
		return user;
	}

	public User findUserById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("id", id));
		User user = (User)criteria.uniqueResult();
		return user;
	}

	public boolean checkUserExist(String username) {
		Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq("username",username));
        long count = criteria.list().size();
        if (count > 0)
            return true;
        return false;
	}

	public List<User> getAllUser() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria critera = session.createCriteria(User.class);
		List<User> users = critera.list();
		return users;
	}

	public int saveUser(User user) {
		return save(user);
	}

	public void updateUser(User user) {
		update(user);	
	}

	public void deleteUser(String name) {
		delete(findUserByName(name));
	}

	public void deleteUser(int id) {
		delete(findUserById(id));
	}

	public List<User> getAllUser(int first, int second)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<User> users = criteria.list();
		return users;
	}

}
