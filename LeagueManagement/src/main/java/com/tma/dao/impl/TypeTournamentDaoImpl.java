package com.tma.dao.impl;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tma.dao.TypeTournamentDao;
import com.tma.model.Tournament;
import com.tma.model.TypeTournament;
@Repository(value = "typetournamentDao")
@Transactional
public class TypeTournamentDaoImpl extends DaoImpl implements TypeTournamentDao
{

	public TypeTournament findById(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TypeTournament.class);
		criteria.add(Restrictions.eq("id", id));
		TypeTournament typeTournament = (TypeTournament) criteria.uniqueResult();
		return typeTournament;
	}

	public TypeTournament findByName(String name)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TypeTournament.class);
		criteria.add(Restrictions.eq("name", name));
		TypeTournament typeTournament = (TypeTournament) criteria.uniqueResult();
		return typeTournament;
	}

	public int saveTypeTournament(TypeTournament typeTournament)
	{
		return save(typeTournament);
	}

	public void updateTypeTournament(TypeTournament typeTournament)
	{
		update(typeTournament);
	}

	public void deleteTypeTournament(String name)
	{
		delete(findByName(name));
	}

	public void deleteTypeTournament(int id)
	{
		delete(findById(id));
	}

	public List<TypeTournament> listAllTypeTournament()
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TypeTournament.class);
		List<TypeTournament> typetournament = criteria.list();
		return typetournament;
	}

	public List<Tournament> findByIdTournament(int id)
	{
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TypeTournament.class);
		TypeTournament typetournament = (TypeTournament)criteria.add(Restrictions.eq("id", id));
		return typetournament.getTournament();
	}

	public List<TypeTournament> listAllTypeTournament(int first, int second) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TypeTournament.class);
		criteria.setFirstResult(first);
		criteria.setMaxResults(second);
		List<TypeTournament> typeTournaments = criteria.list();
		return typeTournaments;
	}

}
