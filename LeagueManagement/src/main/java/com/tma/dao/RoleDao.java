package com.tma.dao;

import java.util.List;

import com.tma.model.Role;

public interface RoleDao {
	Role findById(int id);
	Role findByName(String name);
	int saveRole(Role ur);
	void updateRole(Role ur);
	void deleteRole(int id);
	void deleteRole(String name);
	List<Role>listAll();
	List<Role> listAll(int first, int second);
}
