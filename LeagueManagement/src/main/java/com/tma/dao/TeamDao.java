package com.tma.dao;

import java.util.List;
import java.util.Set;

import com.tma.model.Player;
import com.tma.model.ScoreDetail;
import com.tma.model.Team;

public interface TeamDao
{
	Team findByTeamId(int id);
	Team findByTeamName(String name);
	int saveTeam(Team team);
	void deleteTeamById(int id);
	void deleteTeamByName(String name);
	void updateTeam(Team team);
	List<Player> findPlayerById(int id);
	List<ScoreDetail> findScoreDetailById(int id);
	List<Team> listAll();
	List<Team> listAll(int id);
	List<Team> listAll(int first, int second);
}
