package com.tma.dao;

import java.util.List;
import java.util.Set;

import com.tma.model.Game;
import com.tma.model.Tournament;

public interface GameDao
{
	Game findById(int id);
	Game findByName(String name);
	int saveGame(Game game);
	void updateGame(Game game);
	void deleteGame(String name);
	void deleteGame(int id);
	List<Game> listAllGame();
	List<Game> listAllGame(int first, int second);
	//Set<Tournament> findByIdTournament(int id);
}
