package com.tma.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Location;
import com.tma.model.Matchs;
import com.tma.service.MatchsService;


@Controller
@RequestMapping(value = "/matchs")
public class MatchsController
{
	@Autowired
	MatchsService matchsService;
	
	@RequestMapping(value = "/", method = RequestMethod.PUT, produces = "application/json")
	public void update(@RequestBody Matchs matchs) {
		matchsService.updateMatchs(matchs);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Matchs add(@RequestBody Matchs matchs)
	{
		return matchsService.saveMatchs(matchs);
	}
		
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public void delete(@PathVariable int id)
	{
		matchsService.deleteMatchsById(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Matchs getMatchsById(@PathVariable int id)
	{
		return matchsService.findMatchsById(id);
	}
	@RequestMapping(value = "/all",method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Matchs> getAll()
	{
		return matchsService.listAllMatchs();
	}
	@RequestMapping(value = "/all/start={id1}&end={id2}",method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Matchs> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		return matchsService.listAllMatchs(id1,id2);
	}
}
