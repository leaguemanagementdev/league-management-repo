package com.tma.controller;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Player;
import com.tma.model.Round;
import com.tma.model.Team;
import com.tma.model.Tournament;
import com.tma.service.PlayerService;
import com.tma.service.RoundService;
import com.tma.service.TeamService;
import com.tma.service.TournamentService;

@Controller
@RequestMapping(value = "/tournament")
public class TournamentController {
	@Autowired
	TournamentService tournamentService;

	@Autowired
	TeamService teamService;
	
	@Autowired
	PlayerService playerService;
	
	@Autowired
	RoundService roundService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id, @RequestBody Tournament tournament) {
		Tournament t = tournamentService.findTournamentById(id);
		t.setCreateDate(tournament.getCreateDate());
		t.setGame(tournament.getGame());
		t.setName(tournament.getName());
		t.setNumber(tournament.getNumber());
		t.setRound(tournament.getRound());
		t.setTeam(tournament.getTeam());
		t.setTypetournament(tournament.getTypetournament());
		t.setUser(tournament.getUser());
		tournamentService.updateTournament(t);
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Tournament add(@RequestBody Tournament tournament) {
		Tournament t =  tournamentService.saveTournament(tournament);
		Tournament tx = tournamentService.findTournamentByName(tournament.getName());
		if(tx.getNumber()%2==0)
		{
			for(int i = 0;i<tx.getNumber()-1;i++)
			{
				Round round = new Round("Round"+(i+1),new Date(),tx);
				roundService.saveRound(round);
			}
		}
		else
		{
			for(int i = 0;i<tx.getNumber();i++)
			{
				Round round = new Round("Round"+(i+1),new Date(),tx);
				roundService.saveRound(round);
			}
		}
		for(int i = 0;i<tx.getNumber();i++)
		{
			Team team = teamService.saveTeam(new Team("Team "+rand(),tx));
			Player player = new Player("Player "+rand(),"Thanh Pho HCM - VN","Female","data\1.png",team);
			Player player2 = new Player("Player "+rand(),"Thanh Pho HCM -VN ","Male","data\1.png",team);
			playerService.savePlayer(player);
			playerService.savePlayer(player2);
		}
		
		return t;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void delete(@PathVariable int id) {
		tournamentService.deleteTournament(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Tournament getTournamentById(@PathVariable int id) {
		Tournament tournament = tournamentService.findTournamentById(id);
		return tournament;
	}
	@RequestMapping(value = "/name/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Tournament getTournamentByName(@PathVariable String name) {
		Tournament tournament = tournamentService.findTournamentByName(name);
		return tournament;
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Tournament> getAll() {
		List<Tournament> tournament = tournamentService.getAllTournament();
		return tournament;
	}
	
	
	@RequestMapping(value = "/all/end={id1}&start={id2}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Tournament> getAll(@PathVariable int id1, @PathVariable int id2) {
		List<Tournament> tournament = tournamentService.getAllTournamnet(id1,
				id2);
		return tournament;
	}
	private String rand()
	{
		char[] chars = "ABCDEFGHIJKLMNOPQZWX".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 5; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		String output = sb.toString();
		return output;
	}
}
