package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Round;
import com.tma.model.User;
import com.tma.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController
{
	@Autowired
	UserService userService;
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id,@RequestBody User user) {
		User r = userService.findUserById(id);
		r.setPassword(user.getPassword());
		r.setPathIcons(r.getPassword());
		r.setRole(user.getRole());
		r.setSex(user.getSex());
		r.setTournament(user.getTournament());
		r.setUsername(user.getUsername());
		userService.saveUser(r);
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody User user) throws URISyntaxException
	{
		userService.saveUser(user);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void delete(@PathVariable int id)
	{
		User user = userService.findUserById(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public User getUserById(@PathVariable int id)
	{
		User user = userService.findUserById(id);
		return user;
	}
	@RequestMapping(value = "/all",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<User> getAll()
	{
		List<User> user = userService.getAllUser();
		return user;
	}
	@RequestMapping(value = "/all/end={id1}&start={id2}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<User> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<User> user = userService.getAllUser(id1,id2);
		return user;
	}
}
