package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Game;
import com.tma.service.GameService;

@Controller
@RequestMapping("/game")
public class GameController
{
	@Autowired
	GameService gameService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id,@RequestBody Game game) {
		Game g = gameService.findById(id);
		g.setName(game.getName());
		gameService.updateGame(game);
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody Game game) {
		gameService.saveGame(game);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void deleteGame(@PathVariable int id)
	{
		Game game = gameService.findById(id);
		gameService.deleteGame(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Game getByIdGame(@PathVariable int id)
	{
		Game games = gameService.findById(id);
		return games;
	}
	@RequestMapping(value = "/all",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Game> getAll()
	{
		List<Game> games = gameService.listAllGame();
		return games;
	}
	@RequestMapping(value = "/all/start={id1}&end={id2}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Game> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<Game> games = gameService.listAllGame(id1, id2);
		return games;
	}
	
}
