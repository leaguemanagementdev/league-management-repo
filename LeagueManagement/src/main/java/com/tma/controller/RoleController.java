package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Role;
import com.tma.service.RoleService;
@Controller
@RequestMapping(value = "/roles")
public class RoleController {
	@Autowired
	RoleService roleService;
	@RequestMapping(value = "/{id}",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id,@RequestBody Role role)
	{
		Role rl = roleService.findById(id);
	}
	@RequestMapping(value = "/",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody Role role)
	{
		roleService.saveRole(role);
	}
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void delete(@PathVariable int id)
	{
		Role rl = roleService.findById(id);
		
		roleService.deleteRole(id);
	}
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Role get(@PathVariable int id)
	{
		Role rl = roleService.findById(id);
		return rl;
	}
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Role> getAll()
	{
		List<Role> rls = roleService.listAll();
		return rls;
	}
	@RequestMapping(value = "/all/start={id1}&end={id2}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Role> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<Role> rls = roleService.listAll(id1,id2);
		return rls;
	}
}
