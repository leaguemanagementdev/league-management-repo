package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Tournament;
import com.tma.model.TypeTournament;
import com.tma.service.TournamentService;
import com.tma.service.TypeTournamentService;

@Controller
@RequestMapping(value = "/typetournament")
public class TypeTournamentController
{
	@Autowired
	TypeTournamentService typeTournamentService;
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id,@RequestBody TypeTournament typetournament) {
		TypeTournament t = typeTournamentService.findById(id);
		t.setName(typetournament.getName());
		t.setTournament(typetournament.getTournament());
		typeTournamentService.saveTypeTournament(t);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody TypeTournament typetournament) throws URISyntaxException
	{
		typeTournamentService.saveTypeTournament(typetournament);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void delete(@PathVariable int id)
	{
		TypeTournament typetournament = typeTournamentService.findById(id);
		typeTournamentService.deleteTypeTournament(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public TypeTournament getTypeTournamentById(@PathVariable int id)
	{
		TypeTournament typetournament = typeTournamentService.findById(id);
		return typetournament;
	}
	@RequestMapping(value = "/all",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TypeTournament> getAll()
	{
		List<TypeTournament> typetournament = typeTournamentService.listAllTypeTournament();
		return typetournament;
	}
	@RequestMapping(value = "/all/end={id1}&start={id2}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TypeTournament> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<TypeTournament> typetournament = typeTournamentService.listAllTypeTournament(id1, id2);
		return typetournament;
	}
}
