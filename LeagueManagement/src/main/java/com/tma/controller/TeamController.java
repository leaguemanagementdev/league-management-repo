package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Round;
import com.tma.model.Team;
import com.tma.model.Tournament;
import com.tma.service.TeamService;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

@Controller
@RequestMapping(value="/team")
public class TeamController
{
	@Autowired
	TeamService teamService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id,@RequestBody Team team) {
		
		teamService.updateTeam(team);
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody Team team)
	{
		teamService.saveTeam(team);
		
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void delete(@PathVariable int id)
	{
		teamService.deleteTeamById(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Team getTeamdById(@PathVariable int id)
	{
		Team team = teamService.findTeamById(id);
		return team;
	}
	@RequestMapping(value = "/all",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Team> getAllTeam()
	{
		List<Team> team = teamService.listAll();
		return team;
	}
	@RequestMapping(value = "/all/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Team> getAll(@PathVariable int id)
	{
		List<Team> team = teamService.listAllTeam(id);
		return team;
	}
	@RequestMapping(value = "/all/end={id1}&start={id2}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Team> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<Team> team = teamService.listAllTeam(id1,id2);
		return team;
	}
}
