package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Game;
import com.tma.model.Player;
import com.tma.service.GameService;
import com.tma.service.PlayerService;

@Controller
@RequestMapping(value = "/player")
public class PlayerController
{
	@Autowired
	PlayerService  playerService;
	@RequestMapping(value = "/it")
	public String index()
	{
		return "index";
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id, @RequestBody Player player) {
		Player pl = playerService.findById(id);
		playerService.updatePlayer(player);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody Player player)  {
		playerService.savePlayer(player);
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void deletePlayer(@PathVariable int id)
	{
		Player pl = playerService.findById(id);
		playerService.deletePlayer(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Player getPlayerById(@PathVariable int id)
	{
		Player pl = playerService.findById(id);
		return pl;
	}
	@RequestMapping(value = "/all/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Player> getAll(@PathVariable int id)
	{
		List<Player> pls = playerService.listAllPlayer(id);
		return pls; 
	}
	@RequestMapping(value = "/all/start={id1}&end={id2}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Player> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<Player> pls = playerService.listAllPlayer(id1,id2);
		return pls;
	}
}
