package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Game;
import com.tma.model.Location;
import com.tma.service.LocationService;

@Controller
@RequestMapping(value = "/location")
public class LocationController
{
	@Autowired
	LocationService locationService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id,@RequestBody Location location) {
		Location loca = locationService.findById(id);
		loca.setName(location.getName());
		locationService.updateLocation(loca);
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody Location location) {
		locationService.saveLocation(location);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void delete(@PathVariable int id)
	{
		Location loca = locationService.findById(id);
		locationService.deleteLocation(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Location getLocationById(@PathVariable int id)
	{
		Location loca = locationService.findById(id);
		return loca;
	}
	@RequestMapping(value = "/all",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Location> getAll()
	{
		List<Location> locals = locationService.getAllLocation();
		return locals;
	}
	@RequestMapping(value = "/all/start={id1}&end={id2}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Location> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<Location> locals = locationService.getAllLocation(id1, id2);
		return locals;
	}
}
