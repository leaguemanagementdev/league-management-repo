package com.tma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tma.model.Game;
import com.tma.model.Location;
import com.tma.model.Role;
import com.tma.model.TypeTournament;
import com.tma.model.User;
import com.tma.service.GameService;
import com.tma.service.LocationService;
import com.tma.service.RoleService;
import com.tma.service.TypeTournamentService;
import com.tma.service.UserService;

@Controller
@RequestMapping(value = "/init")
public class InitController
{
	@Autowired
	GameService gameService;
	
	@Autowired
	TypeTournamentService typeTournamentService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	LocationService locationService;
	
	@RequestMapping(value = "/")
	@ResponseBody
	public String init()
	{
		Game game = new Game("Football");
		Game game2 = new Game("Badminton");
		Game game3 = new Game("Tennic");
		gameService.saveGame(game);
		gameService.saveGame(game2);
		gameService.saveGame(game3);
		TypeTournament typeTournament = new TypeTournament("Knockout");
		TypeTournament typeTournament2 = new TypeTournament("Round Robin");
		typeTournamentService.saveTypeTournament(typeTournament);
		typeTournamentService.saveTypeTournament(typeTournament2);
		User user = new User("pvthuan","12345","Male","data\1.png");
		User user2 = new User("bbngoc","12345","Female","data\1.png");
		userService.saveUser(user);
		userService.saveUser(user2);
		Location location = new Location("Yeard A");
		Location location2 = new Location("Yeard B");
		Location location3 = new Location("Yeard C");
		Location location4 = new Location("Yeard D");
		locationService.saveLocation(location);
		locationService.saveLocation(location2);
		locationService.saveLocation(location3);
		locationService.saveLocation(location4);
		return "ok";
	}
}
