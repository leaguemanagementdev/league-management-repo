package com.tma.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tma.model.Round;
import com.tma.service.RoundService;

@Controller
@RequestMapping(value="/round")
public class RoundController {
	@Autowired
	RoundService roundService;
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void update(@PathVariable int id,@RequestBody Round round) {
		Round r = roundService.findRoundById(id);
		
		r.setName(round.getName());
		r.setCreateDate(round.getCreateDate());
		r.setTournament(round.getTournament());
		roundService.updateRound(round);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void add(@RequestBody Round round) throws URISyntaxException
	{
		roundService.saveRound(round);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void delete(@PathVariable int id)
	{
		Round round = roundService.findRoundById(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Round getRoundById(@PathVariable int id)
	{
		Round round = roundService.findRoundById(id);
		return round;
	}
	@RequestMapping(value = "/all",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Round> getAll()
	{
		List<Round> round = roundService.listAllRound();
		return round;
	}
	@RequestMapping(value = "/all/end={id1}&start={id2}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Round> getAll(@PathVariable int id1, @PathVariable int id2)
	{
		List<Round> round = roundService.listAllRound(id1,id2);
		return round;
	}
}
