package com.tma.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "matchs")
public class Matchs implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "matchs_id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "result")
	private int result;
	@Column(name = "score")
	private String score;
	@Column(name = "timematch")
	private Time timeMatch;
	@ManyToOne(optional = false)
	@JoinColumn(name = "location_id")
	private Location location;
	@ManyToOne(optional = false)
	@JoinColumn(name = "team1_id")
	private Team team1;
	@ManyToOne(optional = false)
	@JoinColumn(name = "team2_id")
	private Team team2;
//	@JsonIgnore
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "matchs")
//	private List<Matchs> matchs;
	public Matchs() {
		super();
	}
	
	public Matchs(String name, int result, String score, Time timeMatch, Location location, Team team1, Team team2) {
		super();
		this.name = name;
		this.result = result;
		this.score = score;
		this.timeMatch = timeMatch;
		this.location = location;
		this.team1 = team1;
		this.team2 = team2;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public Time getTimeMatch() {
		return timeMatch;
	}
	public void setTimeMatch(Time timeMatch) {
		this.timeMatch = timeMatch;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public Team getTeam1() {
		return team1;
	}
	public void setTeam1(Team team1) {
		this.team1 = team1;
	}
	public Team getTeam2() {
		return team2;
	}
	public void setTeam2(Team team2) {
		this.team2 = team2;
	}
	
	
}
