package com.tma.model;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "player")
public class Player implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "player_id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "address")
	private String address;
	@Column(name = "sex")
	private String sex;
	@Column(name = "pathIcons")
	private String pathIcons;
	@ManyToOne(optional = false)
	@JoinColumn(name = "team_id")
	private Team team;
	public Player() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Player(String name, String address, String sex, String pathIcons, Team team) {
		super();
		this.name = name;
		this.address = address;
		this.sex = sex;
		this.pathIcons = pathIcons;
		this.team = team;
	}
	public Player(int id, String name, String address, String sex, String pathIcons, Team team) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.sex = sex;
		this.pathIcons = pathIcons;
		this.team = team;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getPathIcons() {
		return pathIcons;
	}
	public void setPathIcons(String pathIcons) {
		this.pathIcons = pathIcons;
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", address=" + address + ", sex=" + sex + ", pathIcons="
				+ pathIcons + ", team=" + team + "]";
	}
	
}
