package com.tma.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "game")
public class Game implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "game_id")
	private int id;
	@Column(name = "name")
	private String name;
	@JsonIgnore
	@OneToMany(cascade= CascadeType.ALL, mappedBy = "game")
	private List<Tournament> tournament;
	public Game() {
		super();
	}
	
	public Game(String name, List<Tournament> tournament) {
		super();
		this.name = name;
		this.tournament = tournament;
	}
	
	public Game(String name) {
		super();
		this.name = name;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Tournament> getTournament() {
		return tournament;
	}
	public void setTournament(List<Tournament> tournament) {
		this.tournament = tournament;
	}
	@Override
	public String toString() {
		return "Game [id=" + id + ", name=" + name + ", tournament=" + tournament + "]";
	}
	
}
