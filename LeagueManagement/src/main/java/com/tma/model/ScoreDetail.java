package com.tma.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "score_detail")
public class ScoreDetail
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "scoreDetail_id")
	private int id;
	@Column(name = "win")
	private int win;
	@Column(name = "lost")
	private int lost;
	@Column(name = "draw")
	private int draw;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "team_id")
	private Team team;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "matchs_id")
	private Matchs matchs;
}
