package com.tma.model;

public enum iRole
{
	USER("USER"),
	DBA("DBA"),
	ADMIN("ADMIN");
	
	String role;
	
	private iRole(String role){
		this.role = role;
	}
	
	public String getUserRole(){
		return role;
	}
}
