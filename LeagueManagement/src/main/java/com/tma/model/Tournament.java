package com.tma.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name = "tournament")
public class Tournament
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="tournament_id")
	private int id;
	@Column(name ="name")
	private String name;
	@Column(name = "numberTeam")
	private int number;
	@Column(name ="createDate")
	private Date createDate;
	@ManyToOne(optional = false)
	@JoinColumn(name = "user_id")
	private User user;
	@ManyToOne(optional = false)
	@JoinColumn(name = "game_id")
	private Game game;
	@ManyToOne(optional = false)
	@JoinColumn(name = "typetournament_id")
	private TypeTournament typetournament;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tournament")
	private List<Round> round;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tournament2")
	private List<Team> team;
	
	public Tournament() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public TypeTournament getTypetournament() {
		return typetournament;
	}

	public void setTypetournament(TypeTournament typetournament) {
		this.typetournament = typetournament;
	}

	public List<Round> getRound() {
		return round;
	}

	public void setRound(List<Round> round) {
		this.round = round;
	}

	public List<Team> getTeam() {
		return team;
	}

	public void setTeam(List<Team> team) {
		this.team = team;
	}	
}
