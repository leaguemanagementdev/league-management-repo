package com.tma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "role_id")
	private int id;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private iRole type = iRole.USER;// db -> 1/2/3/4 ? String?
	// private String type = iRole.USER.getUserRole();

	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}



	public int getId() {
		return id;
	}


	public Role(iRole type) {
		super();
		this.type = type;
	}


	public void setId(int id) {
		this.id = id;
	}


	public iRole getType() {
		return type;
	}


	public void setType(iRole type) {
		this.type = type;
	}


	@Override
	public String toString() {
		return "UserRole [id=" + id + ", type=" + type + "]";
	}

}
