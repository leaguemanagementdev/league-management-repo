package com.tma.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "typetournament")
public class TypeTournament
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "typetournament_id")
	private int id;
	@Column(name="name")
	private String name;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "typetournament")
	private List<Tournament> tournament;
	public TypeTournament() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TypeTournament(String name) {
		super();
		this.name = name;
	}
	
	public TypeTournament(String name, List<Tournament> tournament) {
		super();
		this.name = name;
		this.tournament = tournament;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Tournament> getTournament() {
		return tournament;
	}
	public void setTournament(List<Tournament> tournament) {
		this.tournament = tournament;
	}
	
}
