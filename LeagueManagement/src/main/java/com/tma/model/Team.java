package com.tma.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "team")
public class Team 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "team_id")
	private int id;
	@Column(name = "name")
	private String name;
	@ManyToOne(optional = false)
	@JoinColumn(name = "tournament_id")
	private Tournament tournament2;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "team")
	private List<Player> player;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "team1")
	private List<Matchs> match1;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "team2")
	private List<Matchs> match2;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "team")
	private List<ScoreDetail> scoreDetail;
	public Team() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Team(String name, Tournament tournament2) {
		super();
		this.name = name;
		this.tournament2 = tournament2;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Tournament getTournament2() {
		return tournament2;
	}
	public void setTournament2(Tournament tournament2) {
		this.tournament2 = tournament2;
	}
	public List<Player> getPlayer() {
		return player;
	}
	public void setPlayer(List<Player> player) {
		this.player = player;
	}
	public List<Matchs> getMatch1() {
		return match1;
	}
	public void setMatch1(List<Matchs> match1) {
		this.match1 = match1;
	}
	public List<Matchs> getMatch2() {
		return match2;
	}
	public void setMatch2(List<Matchs> match2) {
		this.match2 = match2;
	}
	public List<ScoreDetail> getScoreDetail() {
		return scoreDetail;
	}
	public void setScoreDetail(List<ScoreDetail> scoreDetail) {
		this.scoreDetail = scoreDetail;
	}
	
}
