package com.tma.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "round")
public class Round implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "round_id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "createDate")
	private Date createDate;
	@ManyToOne(optional = false)
	@JoinColumn(name = "tournament_id")
	private Tournament tournament;
	public Round() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Round(String name, Date createDate, Tournament tournament) {
		super();
		this.name = name;
		this.createDate = createDate;
		this.tournament = tournament;
	}
	
	public Round(String name, Date createDate) {
		super();
		this.name = name;
		this.createDate = createDate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Tournament getTournament() {
		return tournament;
	}
	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}
	@Override
	public String toString() {
		return "Round [id=" + id + ", name=" + name + ", createDate="
				+ createDate + ", tournament=" + tournament + "]";
	}
	
}
