
app.factory('TournamentService', ['$http', function($http) {
	var url = 'http://localhost:8080/LeagueManagement/api/';
	
//	VAR _GET = FUNCTION(NAME){
//		RETURN $HTTP.GET(URL+'/'+NAME);
//	};
//	
//	VAR _POST = FUNCTION(NAME, DATA){
//		RETURN $HTTP.POST(URL +'/'+ NAME, DATA);
//	};
	
//	TournamentService.tournament.post(data);
	
	return {
//		tournament: {
//			get: function(){ return $http.post(url +'/'+ name, data);},
//			post: function(data){ return $http.post(url +'tournament', data);},
//			getByID: getById
//		},
//		
	
		getData: function()
		{
			return $http.get(url+'tournament/'+'all');
		},
		getDataById: function(id){
			return $http.get(url+'tournament/'+id);
		},
		getDataByName: function(name)
		{
			return $http.get(url+'tournament/name/'+name);
		},
		postData: function(data)
		{
			return $http.post(url+'tournament/'+'new',data);
		}
		,
		putData: function(data,id)
		{
			return $http.put(url+'tournament/'+id,data);
		},
		deleteData: function(id)
		{
			return $http.delete(url+'tournament/'+id);
		},
		getDataG: function()
		{
			return $http.get(url+'game/'+'all');
		},
		getDataTT: function()
		{
			return $http.get(url+'typetournament/'+'all');
		}
		,
		getDataU: function()
		{
			return $http.get(url+'user/all');
		},
		getDataTeam: function(id)
		{
			return $http.get(url+'team/all/'+id);
		}
		,
		getDataPlayer: function(id)
		{
			return $http.get(url+'player/all/'+id);
		},
		getDataTeamById: function(id)
		{
			return $http.get(url+'team/'+id);
		},
		putDataTeam: function(id,data)
		{
			return $http.put(url+'team/'+id,data);
		}
	}
} ]);