app.controller('TournamentController',['$scope','TournamentService','$uibModal','$resource',function($scope, TournamentService, $uibModal,$resource) {
		$scope.tournament = {};
		$scope.tournament.game = {};
		$scope.tournament.user = {};
		$scope.tournament.typetournament = {};
		$scope.tournaments = [];
		$scope.games = [];
		$scope.typetournaments = [];
		$scope.users = [];
		$scope.getAll = function() {
			TournamentService.getData().then(function(response) {
					$scope.tournaments = response.data;
				},
				function(errResponse) {
						console.error('Error while loading tournament');
				});
			};
			$scope.getAll();
			$scope.name = $scope.tournaments.name;
			$scope.deleteT = function(id) {
					TournamentService.deleteData(id).then(
							function(response) {
								$scope.getAll();
								location.reload();
							},
							function(errResponse) {
								console.log("Error while deleting tournament");
				});
			};
			function getGameAll() {
					TournamentService.getDataG().
						then(function(response) {
								$scope.games = response.data;
						}, function(errResponse) {
								console.log('Error');
				});
			}
			getGameAll();
			function getTeamAll(id)
			{
				TournamentService.getDataTeam(id).then(function(response){
					$scope.teams = response.data;	
					},
					function(errResponse) {
						console.log('Error');				 
				});
				TournamentService.getDataById(id).then(function(response){
					$scope.t = response.data;
				});
			}
			$scope.openteam = function(id)
			{
					getTeamAll(id);
			}
			$scope.open = function(id) {
				var modalInstance = $uibModal.open({
					animation : $scope.animationsEnabled,
					templateUrl : 'app/js/tournament/template/Modal.html',
					controller : 'ModalCtrl',
					resolve : {
						Action : function() {
							if (id == null || typeof id == 'undefined') {
								return "New";
							}
							else {
									return "Edit";
							}

						},
						ID : function() {
								if (id == null || typeof id == 'undefined') {
									return null;
								} 
								else {
										return id;
									}
								}
						}
				});
		};
		$scope.openplayer = function(id)
		{
			var modalInstance = $uibModal.open({
				animation : $scope.animationsEnabled,
				templateUrl : 'app/js/tournament/template/player.html',
				controller : 'PlayerCtrl',
				resolve : {
					Action: function()
					{
						if(id == null || typeof id == 'undefined') {
							return "New";
						}
						else {
							return "Edit";
						}
					},
					ID : function() {
						return id;
						}
					}
			});
		}
		$scope.openteamdedit = function(id)
		{
			var modalInstance = $uibModal.open({
				animation : $scope.animationsEnabled,
				templateUrl : 'app/js/tournament/template/team.html',
				controller: 'TeamCtrl',
				resolve: {
					Action : function() {
						if (id == null || typeof id == 'undefined') {
							return "New";
						} 
						else {
								return "Edit";
							}
						
					},
					ID: function() {
						return id;
					}
				}
					});
			modalInstance.result.then(function() {
				$scope.getAll();
			}, function() {

		});
		}
//		$scope.addMatchs = function(id)
//		{
//			var modalInstance = $uibModal.open({
//				animation : $scope.animationsEnabled,
//				templateUrl : 'app/js/tournament/template/match.html',
//				controller: 'MatchCtrl',
//				resolve: {
//					Action : function() {
//						if (id == null || typeof id == 'undefined') {
//							return null;
//						} 
//						else {
//								return id;
//							}
//						
//					},
//					ID: function() {
//						return id;
//					}
//				}
//					});
//		}
} ]);
app.controller('ModalCtrl',['$scope','TournamentService','$uibModalInstance','Action','ID',function($scope, TournamentService, $uibModalInstance,Action, ID) {
	$scope.tournament = {};
	$scope.tournament.game = {};
	$scope.tournament.user = {};
	$scope.action = Action;
	function loadData() {
		if (ID) {
			TournamentService.getDataById(ID).
				then(function(respone) {
						$scope.tournament = respone.data;
				});
			}
	};

	loadData();

	function randomString(len, charSet) {
		charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		var randomString = '';
		for (var i = 0; i < len; i++) {
			var randomPoz = Math.floor(Math.random() * charSet.length);
			randomString += charSet.substring(randomPoz,randomPoz+1);
		}
		return randomString;
	}
							
	$scope.submit = function() {
		if (ID) {
			TournamentService.putData($scope.tournament, ID).
				then(function(response) {
					location.reload();
					$uibModalInstance.close(response.data);
			});
		} else {
					TournamentService.postData($scope.tournament).
						then(function(response) {
							location.reload();
							$uibModalInstance.close(response.data);
						});			
		}
	};
	$scope.cancel = function() {
			$uibModalInstance.dismiss('Cancel');
	};
	function getGameAll() {
			TournamentService.getDataG().then(function(response) {
					$scope.games = response.data;
			}, function(errResponse) {
					console.log('Error');
			});
	}
	getGameAll();						
	function getTypeTournamentAll() {
		TournamentService.getDataTT().
			then(function(response) {
				$scope.typetournaments = response.data;
			}, function(errResponse) {
					console.log('Error');

		});
}
	getTypeTournamentAll();
	function getUserAll() {
		TournamentService.getDataU().then(function(response) {
				$scope.users = response.data;
			}, function(errResponse) {
					console.log('Error');
			});
		}
		getUserAll();
} ]);
app.controller('PlayerCtrl',['$scope','TournamentService','$uibModalInstance','Action','ID',function($scope,TournamentService,$uibModalInstance,Action,ID) {
	$scope.action = Action;
	TournamentService.getDataPlayer(ID).then(function(response) {
		$scope.players = response.data;
	},function(errResponse){
		console.log('error');
	});
	TournamentService.getDataTeamById(ID).then(function(response){
		$scope.team = response.data;
	},function(errResponse){
		console.log('error');
	});
	$scope.cancel = function() {
		$uibModalInstance.dismiss('Cancel');
	}
}]);
app.controller('TeamCtrl',['$scope','TournamentService','$uibModalInstance','Action','ID',function($scope,TournamentService,$uibModalInstance,Action,ID){
	$scope.action = Action;
	TournamentService.getDataTeamById(ID).then(function(response){
		$scope.team = response.data;
	$scope.submit = function() {
		if (ID) {
			TournamentService.putDataTeam(ID,$scope.team).
				then(function(response) {
					location.reload();
					$uibModalInstance.close(response.data);
			});
		} else {
					TournamentService.postData($scope.tean).
						then(function(response) {
							$uibModalInstance.close(response.data);
						});			
		}
	};
	});
	$scope.cancel = function() {
		$uibModalInstance.dismiss('Cancel');
	}
}]);