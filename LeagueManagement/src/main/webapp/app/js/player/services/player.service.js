
app.factory('PlayerService', ['$http', function($http) {
	var url = 'http://localhost:8080/LeagueManagement/api/';
	return {
		getData: function()
		{
			return $http.get(url+'player/'+'all');
		},
		getDataById: function(id){
			return $http.get(url+'player/'+id);
		},
		postData: function(data)
		{
			return $http.post(url+'player/'+'new',data);
		}
		,
		putData: function(data,id)
		{
			return $http.put(url+'player/'+id,data);
		},
		deleteData: function(id)
		{
			return $http.delete(url+'player/'+id);
		},
		getDataTeam: function()
		{
			return $http.get(url+'team/'+'all');
		}
	}
} ]);